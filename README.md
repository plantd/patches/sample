# Sample Patch

This is meant to be run with the command `plantctl patch sample`. This is the
equivalent of cloning this repository and executing
`ansible-playbook -i hosts patch.yml`.
